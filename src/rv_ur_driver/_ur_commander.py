import rospy
import actionlib
import sys
import time

from rv_manipulation_driver import ManipulationDriver

from std_msgs.msg import Int8
from geometry_msgs.msg import Twist
from sensor_msgs.msg import JointState

from rv_msgs.msg import ManipulatorState
from rv_msgs.msg import ActuateGripperAction, ActuateGripperResult, ActuateGripperGoal
from rv_msgs.srv import SetCartesianImpedanceResponse

from control_msgs.msg import JointTrajectoryControllerState

from _ur_gripper import URGripper

class URCommander(ManipulationDriver):
  def __init__(self):

    self.move_group = rospy.get_param('~move_group', None)

    if not self.move_group:
      rospy.logerr('Unable to load move_group name from rosparam server path: move_group')
      sys.exit(1)

    ManipulationDriver.__init__(self, None)

    self.gripper = URGripper()

    self.velocity_publisher = rospy.Publisher('/cartesian_velocity_node_controller/cartesian_velocity', Twist, queue_size=1)
    self.recover_on_estop = rospy.get_param('/manipulation_commander/recover_on_estop', True)
    
    # handling e-stop
    rospy.Subscriber('//scaled_pos_traj_controller/state', JointTrajectoryControllerState, self.state_cb)
    self.last_estop_state = 0

  def gripper_cb(self, goal):
    """
    ROS Action Server callback - Actuates the gripper to move to either a fixed width or to grasp an object of specified width
    Args:
        goal (rv_msgs/ActuateGripperActionGoal): the actuation goal for the gripper
    """
    if goal.mode == ActuateGripperGoal.MODE_GRASP:
        result = self.gripper.grasp(goal.width, goal.e_outer if goal.e_outer != 0 else 0.01,
                                              goal.e_inner if goal.e_inner != 0 else 0.01, goal.speed,
                                              goal.force)
        self.gripper_server.set_succeeded(ActuateGripperResult(result=result))

    elif goal.mode == ActuateGripperGoal.MODE_STATIC:
        result = self.gripper.move_to(goal.width, goal.speed)
        rospy.loginfo(result)
        self.gripper_server.set_succeeded(ActuateGripperResult(result=result))

    else:
        self.gripper_server.set_succeeded(ActuateGripperResult(result=1))

  def get_cartesian_manipulability_cb(self, req):
    try:
      joints = self.moveit_commander.get_pose_ik(req.stamped_pose)
      return 1
    except:
      return -1

  def state_cb(self, msg):
    state = ManipulatorState()
    state.ee_pose = self.get_link_pose(self.base_frame, self.ee_frame) 
    state.joint_poses = msg.actual.positions

    self.state_publisher.publish(state)
    