import rospy
import timeit
import actionlib
import numpy as np

from sensor_msgs.msg import JointState
from control_msgs.msg import GripperCommandAction, GripperCommandGoal, GripperCommandResult
from trajectory_msgs.msg import JointTrajectoryPoint
from actionlib_msgs.msg import GoalStatus

from robotiq_2f_gripper_control.msg import Robotiq2FGripper_robot_input

MAX_EXPECTED_VALUE=0.076
MIN_EXPECTED_VALUE=0.005

class URGripper(object):
  def __init__(self):
    self.state = state = JointState()

    self.joint_names = [
      'finger_joint'
    ]
    self.joint_signs = np.array([-1.0, -1.0, 1.0, -1.0, 1.0, 1.0])

    self.min_gap = rospy.get_param('/robotiq_2f_gripper_action_server/min_gap', 0)
    self.max_gap = rospy.get_param('/robotiq_2f_gripper_action_server/max_gap', 0.085)

    self.min_effort = rospy.get_param('/robotiq_2f_gripper_action_server/min_effort', 30)
    self.max_effort = rospy.get_param('/robotiq_2f_gripper_action_server/max_effort', 100)

    self.dist_per_tick = (self.max_gap - self.min_gap) / 255.0
    self.effort_per_tick = (self.max_effort - self.min_effort) / 255.0

    self.gripper_client = actionlib.SimpleActionClient('/gripper_controller/gripper_cmd', GripperCommandAction)
    
    self.gripper_state_publisher = rospy.Publisher('arm/gripper/state', JointState, queue_size=1)
    rospy.Subscriber('/Robotiq2FGripperRobotInput', Robotiq2FGripper_robot_input, self.state_cb)
    
    self.DEFAULT_FORCE = rospy.get_param('/gripper_controller/max_effort', 100)
    self.MAX_GRIPPER_VALUE = rospy.get_param('/gripper/max_commanded_width', MAX_EXPECTED_VALUE)
    self.MIN_GRIPPER_VALUE = rospy.get_param('/gripper/min_commanded_width', MIN_EXPECTED_VALUE)

    rospy.loginfo('GRIPPER MAX WIDTH: %f', self.MAX_GRIPPER_VALUE)

  def move_to(self, width, speed=0.1):
    width = (self.MAX_GRIPPER_VALUE / MAX_EXPECTED_VALUE) * width
    width = min(self.MAX_GRIPPER_VALUE, max(self.MIN_GRIPPER_VALUE, self.MAX_GRIPPER_VALUE - width))
    return self._send_goal(width).reached_goal

  def grasp(self, width=0, e_inner=0.01, e_outer=0.01, speed=0.1, force=100):
    # width = (self.MAX_GRIPPER_VALUE / MAX_EXPECTED_VALUE) * width
    # width = min(self.MAX_GRIPPER_VALUE, max(self.MIN_GRIPPER_VALUE, self.MAX_GRIPPER_VALUE - width))
    rospy.loginfo('{} > {}'.format(width, self.state.position[0]))
    result = self._send_goal(
      self.MIN_GRIPPER_VALUE if width >= self.state.position[0] else self.MAX_GRIPPER_VALUE, 
      force if force != 0 else self.DEFAULT_FORCE
    )
    
    rospy.loginfo('{} < {} < {} == {}'.format(width - e_inner, self.state.position[0], width + e_outer, width - e_inner < self.state.position[0] < width + e_outer))
    return width - e_inner < self.state.position[0] < width + e_outer
    
  def state_cb(self, msg):
    state = JointState()

    state.header.stamp = rospy.Time.now()

    state.name = self.joint_names

    state.position = [self.max_gap - (msg.gPO * self.dist_per_tick)]
    state.effort = [self.max_effort - (msg.gCU * self.effort_per_tick)]

    self.gripper_state_publisher.publish(state)
    self.state = state


  def _send_goal(self, width, force=100):
    goal = GripperCommandGoal()
    goal.command.position = width
    goal.command.max_effort = force
    
    self.gripper_client.send_goal(goal)
    
    while self.gripper_client.get_state() <= actionlib.GoalStatus.ACTIVE:
      rospy.sleep(0.01)
    
    result = self.gripper_client.get_result()
    return result 
    